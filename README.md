# simple digital menu

Das simple digital menu (sdm) ist eine sehr kleine und effiziente digitale Speisekarte.

Es ist in der Regel (je nach Menge der Speisen) nur ca. 20 Kilobyte groß. Im Vergleich: Die Startseite von Google ist ca. 19 Megabyte groß, also fast das Tausendfache der SDM.

Damit eignet sich das sdm auch sehr gut in Gebieten mit starker Auslastung der mobilen Netze (z.B. bei großen Veranstaltungen) oder in Gebieten mit schlechtem Empfang.

Aktuell ist es nur zur Anzeige der Speisen und Getränke gedacht, es beinhaltet kein Bestellsystem. Theoretisch wäre aber die Integration in ein Bestellsystem denkbar.

## Systemvorraussetzungen
- Webserver oder Webspace

Da das sdm nur aus einer HTML Datei besteht, benötigt sie nichts weiter als einen Webserver bzw. einen ganz einfachen Webspace bei einem beliebigen Anbieter. Sie braucht kein PHP, keine Datenbank wie MySQL, MariaDB oder PostgreSQL.

## Installation
1. Lade dir die neueste Version herunter: https://gitlab.com/BareCoder/simple-digital-menu/-/archive/main/simple-digital-menu-main.zip oder https://gitlab.com/BareCoder/simple-digital-menu/-/archive/main/simple-digital-menu-main.tar.gz
2. Entpacke die Dateien in ein beliebiges Verzeichnis auf deinem Rechner.
3. Öffne die index.html im Verzeichnis html/menu1 mit einem HTML- oder Texteditor und folge den Anweisungen in den dortigen Kommentaren.
4. Teste deine Änderungen indem du die bearbeitete index.html in einem Browser öffnest (normalerweise reicht dazu ein Doppelklick).
5. Ist alles fertig bearbeitet, lade alles aus dem Verzeichnis "html" auf deinen Webspace.
6. Fertig :) Viel Spaß mit deinem "simple digital menu"!